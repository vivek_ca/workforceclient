# README #

# **Workforce Client Android App** #

Workforce Client Android app act as a client which will send messages to the server mobile app. 

When the app is launched for the first time, it will pop an settings dialog where the server mobile can be entered and the duration interval between each SMS can be set(By default it is 10 mins). Once the server details are entered it is stored in the shared preference.

Once the start button is clicked, the app starts to send SMS message for every selected duration. An Android Service class is started, which has a timer that will trigger an SMS for every constant selected duration. At the time of sending the sending the message all the required fields like (CellID, Location Area Code, Mobile Country Code and Mobile Network Code) are captured and a JSONObject is created with these fields and then the SMS is sent.

## External Libraries Used ##

1. Android Support Library (**Appcompat-v7:22.2.1, Design:22.2.0**)

### Tested Mobile ###

* Nexus 5 (Android 6.0)