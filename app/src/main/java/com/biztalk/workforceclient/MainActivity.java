package com.biztalk.workforceclient;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    Button mStartStopBtn;
    TextView mStatusTxt;
    boolean mServiceStatus = false;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;
    Intent myIntent;
    int mSeekProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        setupViews();
    }

    /**
     * Function to initialize and set up all the views from the layout
     */
    private void setupViews() {
        mStartStopBtn = (Button) findViewById(R.id.start_button);
        mStatusTxt = (TextView) findViewById(R.id.status_text);

        mStartStopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!mServiceStatus) {
                    mServiceStatus = true;
                    mStartStopBtn.setText(getString(R.string.stop));
                    startService();

                } else {
                    mServiceStatus = false;
                    mStartStopBtn.setText(getString(R.string.start));
                    stopService(myIntent);
                }

            }
        });
        checkSharedPreference();
    }

    /**
     * Function to check preference whether any server mobile number is stored
     */
    private void checkSharedPreference() {
        String mobileNo = sharedpreferences.getString(WorkforceConstants.SERVER_MOBILE, WorkforceConstants.NULL);

        if(mobileNo.equals(WorkforceConstants.NULL) || mobileNo.trim().length() != 10) {
            mStartStopBtn.setEnabled(false);
            mStatusTxt.setText("No Server Mobile number is set. Please click settings button.");
            showSettingsDialog();
        }
        else {
            mStartStopBtn.setEnabled(true);
            mStatusTxt.setText("Server Mobile number: " + mobileNo + " is set.");
        }
    }

    /**
     * Function to start the service
     */
    private void startService() {
        myIntent = new Intent(this, SMSSenderService.class);
        startService(myIntent);
    }

    /**
     * Initialize the contents of the Activity's standard options menu.  You
     * should place your menu items in to <var>menu</var>.
     * <p/>
     * <p>This is only called once, the first time the options menu is
     * displayed.  To update the menu every time it is displayed, see
     * {@link #onPrepareOptionsMenu}.
     * <p/>
     * <p>The default implementation populates the menu with standard system
     * menu items.  These are placed in the {@link Menu#CATEGORY_SYSTEM} group so that
     * they will be correctly ordered with application-defined menu items.
     * Deriving classes should always call through to the base implementation.
     * <p/>
     * <p>You can safely hold on to <var>menu</var> (and any items created
     * from it), making modifications to it as desired, until the next
     * time onCreateOptionsMenu() is called.
     * <p/>
     * <p>When you add items to the menu, you can implement the Activity's
     * {@link #onOptionsItemSelected} method to handle them there.
     *
     * @param menu The options menu in which you place your items.
     * @return You must return true for the menu to be displayed;
     * if you return false it will not be shown.
     * @see #onPrepareOptionsMenu
     * @see #onOptionsItemSelected
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     * This hook is called whenever an item in your options menu is selected.
     * The default implementation simply returns false to have the normal
     * processing happen (calling the item's Runnable or sending a message to
     * its Handler as appropriate).  You can use this method for any items
     * for which you would like to do processing without those other
     * facilities.
     * <p/>
     * <p>Derived classes should call through to the base class for it to
     * perform the default menu handling.</p>
     *
     * @param item The menu item that was selected.
     * @return boolean Return false to allow normal menu processing to
     * proceed, true to consume it here.
     * @see #onCreateOptionsMenu
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings)
            showSettingsDialog();

        return super.onOptionsItemSelected(item);
    }

    /**
     * Function to show the settings dialog
     */
    private void showSettingsDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.set_server_mobile);

        View v = getLayoutInflater().inflate(R.layout.settings_layout, null);

        final EditText mServerMobile = (EditText) v.findViewById(R.id.mobile_number);
        mServerMobile.setInputType(InputType.TYPE_CLASS_PHONE);

        int maxLength = 10;
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(maxLength);
        mServerMobile.setFilters(FilterArray);

        mServerMobile.setHint(getString(R.string.server_mobile));
        mServerMobile.setText(sharedpreferences.getString(WorkforceConstants.SERVER_MOBILE, ""));

        final SeekBar mSeekBar = (SeekBar) v.findViewById(R.id.seekBar);
        final TextView mInterValDurationTxt = (TextView) v.findViewById(R.id.hint_name);

        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {
                mSeekProgress = progress;

                mInterValDurationTxt.setText(getString(R.string.sms_interval) + " " + WorkforceConstants.
                        getSMSIntervalDuration(progress));
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mInterValDurationTxt.setText(getString(R.string.sms_interval) + " " + WorkforceConstants.getSMSIntervalDuration(
                sharedpreferences.getInt(WorkforceConstants.INTERVAL_DURATION, 4)));

        mSeekBar.setProgress(sharedpreferences.getInt(WorkforceConstants.INTERVAL_DURATION, 4));

        builder.setView(v);

        builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (mServerMobile.getText().length() == 10) {
                    sharedpreferences.edit().putString(WorkforceConstants.SERVER_MOBILE,
                            mServerMobile.getText().toString()).commit();
                    sharedpreferences.edit().putInt(WorkforceConstants.INTERVAL_DURATION,
                            mSeekBar.getProgress()).commit();
                    checkSharedPreference();
                } else {
                    Snackbar.make(mStartStopBtn, "Please enter valid mobile number.",
                            Snackbar.LENGTH_SHORT).show();
                }
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();

    }
}
