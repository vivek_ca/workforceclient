package com.biztalk.workforceclient;

/**
 * Created by hl0395 on 12/1/16.
 */
public class WorkforceConstants {

    public static final String SERVER_MOBILE = "SERVER_MOBILE";
    public static final String INTERVAL_DURATION = "INTERVAL_DURATION";
    public static final String NULL = "null";

    public static String  getSMSIntervalDuration(int progress){
        switch (progress) {
            case 0:
                return "1 Min";
            case 1:
                return "2 Mins";
            case 2:
                return "5 Mins";
            case 3:
                return "7 Mins";
            case 4:
                return "10 Mins";
        }
        return null;
    }
}
