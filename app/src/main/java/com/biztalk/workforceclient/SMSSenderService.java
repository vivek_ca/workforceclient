package com.biztalk.workforceclient;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by hl0395 on 12/1/16.
 */
public class SMSSenderService extends Service {

    Timer timerSendSMS = new Timer();
    SharedPreferences sharedpreferences;

    class taskSendSMS extends TimerTask {
        @Override
        public void run() {
            hSendSMS.sendEmptyMessage(0);
        }
    };

    final Handler hSendSMS = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            procSendSMS();
            return false;
        }
    });

    /**
     * Function to send SMS
     */
    public void procSendSMS() {
        try {

            TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
            GsmCellLocation cellLocation = (GsmCellLocation)telephonyManager.getCellLocation();
            String carrierName = telephonyManager.getNetworkOperatorName();

            String networkOperator = telephonyManager.getNetworkOperator();
            String mcc = networkOperator.substring(0, 3);
            String mnc = networkOperator.substring(3);

            JSONObject smsMsgObject =new JSONObject();
            smsMsgObject.put("carrier", carrierName);
            smsMsgObject.put("cellID", cellLocation.getCid());
            smsMsgObject.put("LAC", cellLocation.getLac());
            smsMsgObject.put("mcc", mcc);
            smsMsgObject.put("mnc", mnc);

            // send your SMS here
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage(sharedpreferences.getString(WorkforceConstants.SERVER_MOBILE,"9739344416"), null,
                    smsMsgObject.toString(),null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onCreate() {
        super.onCreate();
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);

            int mDuration = 10;
            switch (sharedpreferences.getInt(WorkforceConstants.INTERVAL_DURATION,4)) {
                case 0:
                    mDuration = 1;
                    break;
                case 1:
                    mDuration = 2 ;
                    break;
                case 2:
                    mDuration = 5 ;
                    break;
                case 3:
                    mDuration = 7;
                    break;
                case 4:
                    mDuration = 10;
                    break;
            }

            long intervalSendSMS = mDuration * 60 * 1000;
            timerSendSMS = new Timer();
            timerSendSMS.schedule(new taskSendSMS(), 0, intervalSendSMS);

        } catch (NumberFormatException e) {
            Toast.makeText(this, "error running service: " + e.getMessage(),
                    Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(this, "error running service: " + e.getMessage(),
                    Toast.LENGTH_SHORT).show();
        }

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onDestroy() {

        timerSendSMS.cancel();
        timerSendSMS.purge();

    }
}
